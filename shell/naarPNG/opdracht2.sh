#!/usr/bin/env bash
list=$(find -type f -name *.jpg)
for val in $list; do
    IFS='./'
    read -a filearr <<< "$val"
    #${filearr[2]} is naam ${filearr[3]} is extensie
    eval convert ${filearr[2]}.${filearr[3]} -resize 16384@ ${filearr[2]}.png
    #eval ls
done