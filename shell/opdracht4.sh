#!/usr/bin/env bash
if [ "$1" = "passwordfile" ]; then
    echo "Gebruikersnaam: "
    read name;

    if [ -z "$name" ]; then
        echo "empty"
        name="$(whoami)"
    fi
    echo "jij bent $name"

    #ww
    oudwachtwoord=""
    nieuwwachtwoord=""
    while [ -z $oudwachtwoord ]
    do
        echo "uw wachtwoord: "
        read oudwachtwoord;
        if [ ${#oudwachtwoord} -ge 8 ];then 
            echo "wachtwoord is lang genoeg"
        else 
            echo "wachtwoord is te kort"
            oudwachtwoord=""
        fi
    done
    while [ "$oudwachtwoord" != "$nieuwwachtwoord" ]
    do
        if [ -z $nieuwwachtwoord ];
        then 
            echo "wachtwoord ter herhaling: "
            read nieuwwachtwoord;
        else
            echo "wachtwoorden komen niet overeen, vul nogmaals in: "
            read nieuwwachtwoord;
        fi
    done
    hash=$( echo $oudwachtwoord | md5sum | awk '{print $oudwachtwoord}')
    echo $hash
    datafile="$name --- $hash"
    echo $datafile >> $1 
else
    echo "geef de juiste filenaam mee!"
fi