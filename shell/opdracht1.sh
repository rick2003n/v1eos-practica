#!/usr/bin/env bash
IFS='.'
read -a strarr <<< "$1"

case ${strarr[1]} in #extensie pakken
    py) #python
        eval python3 ${strarr[0]}.${strarr[1]}
    ;;
    cc) #c
        eval cat ${strarr[0]}.${strarr[1]}
    ;;
    sh) #shell script
        eval bash ${strarr[0]}.${strarr[1]}
    ;;
    *) #error
        echo "extensie niet gevonden"
    ;;
esac