#!/usr/bin/env bash

> $2
for file in $(find .); do
        if $1 "$file" >/dev/null 2>&1; then
                echo "$file = op deze file werkt het commando $1 wel" >> $2
        else
                echo "$file = op deze file werkt het commando $1 niet" >> $2
        fi
done
cat $2