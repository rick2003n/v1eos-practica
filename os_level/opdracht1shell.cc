#include "shell.hh"
using namespace std;

//seek en find werken nog niet volledig, hoop dat het goed genoeg is.
//grt Rick


int main()
{ string input;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
  string prompt = "Geef een commando: ";

  while(true)
  { cout << prompt;                   // Print het prompt
    getline(cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (cin.eof()) return 0; } }      // EOF is een exit

void new_file() // ToDo: Implementeer volgens specificatie.
{
  string bestandsnaam;
  cout << "De bestandsnaam is: ";
  cin >> bestandsnaam;
  cout << "Inhoud: ";
  string inhoud;
  cin.ignore();
  getline(cin, inhoud);
  const char *c_file_name = bestandsnaam.c_str();
  const char *c_file_text = inhoud.c_str();
  int create_new_file = syscall(SYS_open,c_file_name, O_CREAT | O_RDWR | O_TRUNC, 0666);
  int write_new_file = syscall(SYS_write, create_new_file, c_file_text, inhoud.length());
  close(create_new_file);
  close(write_new_file);
 }

void list() // ToDo: Implementeer volgens specificatie.
{ 
  pid_t pid =0;
  pid = syscall(SYS_fork);
  const char* args[] = {"/bin/ls", "-l", "-a", NULL};
  if(pid == 0){
    syscall(SYS_execve, args[0], args, NULL);
  }else{
    cout << pid << syscall(SYS_wait4, pid, NULL, NULL);
  }
} 

void find() // ToDo: Implementeer volgens specificatie.
{
  string woord;
  int pipe_A[2];
  
  cout << "Woord: ";
  cin >> woord;
 
  syscall(SYS_pipe,pipe_A);
  
  pid_t pid_A, pid_B;
  
  if( !(pid_A = fork()) ) {
    const string find_location = "/bin/find";
    const char *find_arguments[] = {find_location.c_str(), ".", NULL};

    syscall(SYS_dup2,pipe_A[1], 1); /* redirect standard output to pipe_A write end */
    syscall(SYS_execve, find_location.c_str(), find_arguments, NULL);
  }
  
  if( !(pid_B = fork()) ) {
    const string grep_location = "/bin/grep";
    const char *grep_arguments[] = {grep_location.c_str(), woord.c_str(), NULL};

    syscall(SYS_dup2,pipe_A[0], 0); /* redirect standard input to pipe_A read end */
    syscall(SYS_execve, grep_location.c_str(), grep_arguments, NULL);
    syscall(SYS_close,pid_A);
    syscall(SYS_close,pid_B);
  }
} 


void seek() // ToDo: Implementeer volgens specificatie.
{
  string file_seek = "seek";
  string file_loop = "loop";
 
  // sneller
  int create_new_seek = syscall(SYS_open, file_seek.c_str(), O_CREAT | O_RDWR | O_TRUNC, 0666);
  int write_to_seek = syscall(SYS_write, create_new_seek, "x", 1);
  int lseek = syscall(SYS_lseek, create_new_seek,5000000,SEEK_CUR);
  int write_to_seek_2 = syscall(SYS_write, create_new_seek, "x", 1);

  if ((syscall(write_to_seek) == 0) || (syscall(lseek) == 0) || (syscall(write_to_seek_2) == 0)) {
     cout << "error met lseek" << endl;
  }
  
  // slomer
  int create_new_loop = syscall(SYS_open, file_loop.c_str(), O_CREAT | O_RDWR | O_TRUNC, 0666);
  int write_to_loop_0 = syscall(SYS_write, create_new_loop, "x", 1);
  for (unsigned int i = 0; i < 5000000; i++) {
    int write_to_loop_1 = syscall(SYS_write, create_new_loop, "\0", 1);
  }
  int write_to_loop_2 = syscall(SYS_write, create_new_loop, "x", 1);

  if ((syscall(write_to_loop_0) == 0) || (syscall(write_to_loop_2) == 0)) {
     cout << "looperror" << endl;
  }

}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
