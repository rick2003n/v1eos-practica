#include <iostream>
#include <string>
using namespace std; 

string welkom(string naam)
{
	string result = "Welkom, " + naam + " :-)" +'\n';
  return result;
 }
int main(int argc, char *argv[])
{ if(argc != 2)
  { cerr << "Deze functie heeft exact 1 argument nodig" << endl;
    return -1; 
  }
  else{
    cout << welkom(argv[1]);
  }

} 
